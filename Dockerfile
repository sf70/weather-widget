FROM node:18.12.1
WORKDIR /app
ADD package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn run build

FROM nginx:stable-alpine
COPY --from=0 --chown=nginx /app/dist /usr/share/nginx/html
COPY --from=0 --chown=nginx /app/build/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 8080