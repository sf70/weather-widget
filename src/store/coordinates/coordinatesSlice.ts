import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../store';
import config from "../../api/config";

// import { fetchCount } from './counterAPI';

export interface State {
  value:  Array<number>;
  status: 'idle' | 'loading' | 'failed';
}

// const initialState = createInitialState();

const initialState: State = {
  value: [0, 0],
  status: 'idle',
};

export const getIPInfo = createAsyncThunk(
  //action type string
  'coordinates/getPosts',
  async () => {
    const url = new URL("https://ipinfo.io/json");
    url.search = new URLSearchParams({
      token: config.GEO_API_KEY,
    }).toString();
    try {
      const response = await fetch(url);
      if (response.status === 200) {
        const jsonResponse = await response.json();
        return [+jsonResponse.loc.split(',')[0], +jsonResponse.loc.split(',')[1]];
      }
      return [0,0];
    } catch {
      return [0,0];
    }
  }
)

export const coordinatesSlice = createSlice({
  name: 'coordinates',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setCoordinates: (state, action: PayloadAction<Array<number>>) => {
      state.value = action.payload;
    },
  },
  extraReducers: builder => {
    builder
    .addCase(getIPInfo.pending, (state) => {
      state.status = 'loading';
      console.log('loading')
    })
    .addCase(getIPInfo.fulfilled, (state, action) => {
      state.status = 'idle';
      console.log('idle')
      if (state.value[0] === 0 && state.value[1] === 0) {
        state.value = action.payload;
      }
    })
    .addCase(getIPInfo.rejected, (state) => {
      console.log('failed')
      state.status = 'failed';
    });
  }
});

export const { setCoordinates } = coordinatesSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectCoordinates = (state: RootState) => state.coordinates.value;
export const notNullCords = (state: RootState) => {
  if (state.coordinates.value[0] != 0 && state.coordinates.value[1] != 0) {
    return true
  }
  return false
}
// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.


export default coordinatesSlice.reducer;
