import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import themeReducer from './theme/themeSlice';
import coordinatesReducer from './coordinates/coordinatesSlice';
import userinputReduser from './userinput/userinputSlice';
import citiesReduser from './cities/citiesSlice';
import weatherReduser from './weather/weatherSlice';
import weatherArrayReduser from './weatherarray/weatherarraySlice';
import cityReduser from './city/citySlice';
import fiveDaysReducer from './fiveDays/fiveDaysSlice';

// import themeReducer from '../features/theme/themeSlice';

export const store = configureStore({
  reducer: {
    theme: themeReducer,
    coordinates: coordinatesReducer,
    userinput: userinputReduser,
    cities: citiesReduser,
    weather: weatherReduser,
    weatherArray: weatherArrayReduser,
    city: cityReduser,
    fiveDays: fiveDaysReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
