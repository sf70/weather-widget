import Header from 'Components/Header';
import Main from 'Components/Main';

import styles from './index.module.scss';

function App() {
  return (
    <div className={styles.app}>
      <Header />
      <Main />
    </div>
  );
}

export default App;
