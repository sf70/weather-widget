import styles from './index.module.scss'
import Map from 'Components/Map';
import WeatherCard from 'Components/WeatherCard';
import { useAppSelector, useAppDispatch } from 'store/hooks';
import { selectWeatherArray } from 'store/weatherarray/weatherarraySlice';
import { selectUserInput } from 'store/userinput/userinputSlice';
import { getWeather } from 'store/weatherarray/weatherarraySlice';
import { selectCities } from 'store/cities/citiesSlice';
import { selectCoordinates } from 'store/coordinates/coordinatesSlice';
import { getCityByPosition, selectCity } from 'store/city/citySlice';
import { useEffect } from "react";

export function Main() {

  const weatherArray = useAppSelector(selectWeatherArray);
  const city = useAppSelector(selectCity);
  const dispatch = useAppDispatch();
  const userInput = useAppSelector(selectUserInput);
  const cities = useAppSelector(selectCities);
  const coordinates = useAppSelector(selectCoordinates);

  useEffect(() => {
    if (coordinates[0] != 0 && coordinates[1] != 0) {
      dispatch(getCityByPosition([coordinates[0], coordinates[1]]))
      dispatch(getWeather([coordinates[0], coordinates[1]]))
    }
  }, [ coordinates ])

  return (
    <main className={styles.main}>
      <Map />
      {weatherArray.list.length != 0 &&  city ? <WeatherCard /> : <></>}
    </main>
  )
}

export default Main