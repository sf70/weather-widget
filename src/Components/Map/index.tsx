import { MapContainer, TileLayer, Marker, useMap } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import icon from "./loc.png";
import L from "leaflet";
import styles from './index.module.scss'
import './styles.scss'
import { useAppSelector, useAppDispatch } from 'store/hooks';
import { selectCities } from 'store/cities/citiesSlice';

import { selectCoordinates, getIPInfo, setCoordinates, notNullCords } from '../../store/coordinates/coordinatesSlice';
import { useEffect, useState } from "react";

function Map() {
  const coordinates = useAppSelector(selectCoordinates);
  const notZeroCords = useAppSelector(notNullCords);
  const dispatch = useAppDispatch();
  const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

  const geo = setTimeout(function () {
    if (coordinates[0] === 0 && coordinates[1] === 0) {
      console.log("getIPInfo")
      dispatch(getIPInfo());
    }
  }, 1000);

  const geoPositionSuccess = (position: GeolocationPosition) => {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    dispatch(setCoordinates([latitude, longitude]))
    clearTimeout(geo)
  }

  if (coordinates[0] === 0 && coordinates[1] === 0) {
    navigator.geolocation.getCurrentPosition(geoPositionSuccess, null);
  }

  function MapView() {
    let map = useMap();
    if (coordinates[0] === 0 && coordinates[1] === 0) {
      map.setView([0, 0], 3)
    } else {
      windowWidth <= 1140 && map.setView([coordinates[0], coordinates[1]], 10)
      windowWidth > 1140 && windowWidth <= 1420 && map.setView([coordinates[0], coordinates[1] + (width / 100 * 25 / 600)], 10)
      windowWidth > 1420 && map.setView([coordinates[0], coordinates[1] + (width / 100 * 25 / 800)], 10)
    }
    return (<></>);
  }

  const customIcon = new L.Icon({
    iconUrl: icon,
    iconSize: [35, 35],
    iconAnchor: [5, 30]
  });

  function Cleaner() {
    setTimeout(() => {
      let btm = document.getElementsByClassName("leaflet-control-attribution leaflet-control")[0];
      let link = btm.getElementsByTagName("a")[0];
      link.innerHTML = '';
      link.text = "Leaflet";
    }, 1);
    return (<></>)
  }

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    const handleWindowResize = () => {
      setWindowWidth(window.innerWidth);
    };
  
    window.addEventListener('resize', handleWindowResize);
    
    
    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  });

  return (
    <div className={styles.map}>
      <MapContainer
        center={[coordinates[0], coordinates[1]]}
        zoom={10}
        scrollWheelZoom={true}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {notZeroCords ? <Marker icon={customIcon} position={[coordinates[0], coordinates[1]]} /> : <></>}
        <MapView />
        <Cleaner />
      </MapContainer>
    </div>
  );
}

export default Map