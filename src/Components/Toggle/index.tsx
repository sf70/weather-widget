import React, { useEffect } from 'react'
import styles from './index.module.scss'
import { useAppSelector, useAppDispatch } from '../../store/hooks';
import { themeChange, selectTheme } from 'store/theme/themeSlice';

function Toggle() {
  const theme = useAppSelector(selectTheme);
  const dispatch = useAppDispatch();
  useEffect(() => {
    document.documentElement.dataset.theme = theme
    localStorage.setItem('theme', theme)
  }, [ theme ])
  return (
    <label className={styles.root} htmlFor="toggler">
      <input
        id="toggler"
        type="checkbox"
        onClick={()=>dispatch(themeChange())}
        checked={theme == 'dark'}
        readOnly
      />
      <span className={styles.slider} />
      <span className={styles.wave} />
    </label>
  )
}


export default Toggle