import styles from './index.module.scss'
import { useAppSelector, useAppDispatch } from 'store/hooks';
import React from 'react';
import { selectUserInput, setUserInput } from 'store/userinput/userinputSlice';
import { getCities, selectCities, setCities } from 'store/cities/citiesSlice';
import { selectCoordinates, setCoordinates } from 'store/coordinates/coordinatesSlice';
import { selectCity, selectCityName } from 'store/city/citySlice';
import { useEffect } from "react";



export function Input() {

  const dispatch = useAppDispatch();
  const userInput = useAppSelector(selectUserInput);
  const cities = useAppSelector(selectCities);
  const cityName = useAppSelector(selectCityName);
  const city = useAppSelector(selectCity);

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setUserInput(e.currentTarget.value));
    if (e.currentTarget.value.length != 0) {
      dispatch(getCities(e.currentTarget.value));
    } else {
      dispatch(setCities([]))
    }
    e.stopPropagation()
  };
  useEffect(() => {
    if (city) {
      dispatch(setUserInput(cityName));
    }
  }, [ cityName ])


  const handleInputChoice = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, id: number) => {
    dispatch(setCoordinates([cities[id].lat!, cities[id].lon!]));
    dispatch(setCities([]));
    event.stopPropagation()
  };

  return (
    <div className={styles.inputContainer}>
      <input 
        type="text"
        placeholder='Введите название города'
        className={styles.maininput}
        onInput={handleInput}
        value={userInput}
      />
      <div className={styles.select_container}>
      {cities.map((city, index) => (
        <div
          key={index}
          className={styles.select}
          onClick={(event) => handleInputChoice(event, index)}
        >
          {city.name} - {city.state},{city.country}
        </div>
      ))}
      </div>
    </div>
  )
}

export default Input