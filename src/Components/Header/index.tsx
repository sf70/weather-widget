import Toggle from 'Components/Toggle'
import Input from 'Components/Input'
import Logo from 'Components/Logo'
import styles from './index.module.scss'
import { useEffect, useState } from "react"


export function Header() {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  
  useEffect(() => {
    const handleWindowResize = () => {
      setWindowWidth(window.innerWidth);
    };
  
    window.addEventListener('resize', handleWindowResize);
  
    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  });

  return (
    <header className={styles.header}>
      {windowWidth >=1140 && <Logo />}
      <Input />
      <Toggle />
    </header>
  )
}

export default Header