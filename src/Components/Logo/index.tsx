import styles from './index.module.scss'
import { selectTheme } from 'store/theme/themeSlice';
import { useAppSelector } from '../../store/hooks';

export function Logo() {
  const theme = useAppSelector(selectTheme);

  return (
    <div>
      
      { theme === 'dark' ? 
      <svg className={styles.logo} width="800px" height="800px" viewBox="0 0 32 32" id="umbrellawithraindrops_Dark" data-name="icon-ark" xmlns="http://www.w3.org/2000/svg">
        <path id="Path" d="M0,0H2V2H0Z" fill="#414141"/>
        <path id="Path-2" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2)" fill="#fafafa"/>
        <path id="Path-3" data-name="Path" d="M4,0H8V2h2V0h4V2H12V4H10V6H8V8h2V6h2V4h2V2h2V0h4V2H18V4H16V6h2V4h2V2h2V0h4V2h2V0h4V2H30V4H28V6H26V8H24v2h2V8h2V6h2V4h2V8H30v2H28v2h2v2H28v2H26V14H24V12H20V10H16V8H14v2H10v2H6V10H4v2H2v2H0V10H2V8H0V4H2V2H4Z" fill="#414141"/>
        <path id="Path-4" data-name="Path" d="M0,0H2V2H0Z" transform="translate(8)" fill="#fafafa"/>
        <path id="Path-5" data-name="Path" d="M0,0H2V2H0Z" transform="translate(14)" fill="#fafafa"/>
        <path id="Path-6" data-name="Path" d="M0,0H2V2H0Z" transform="translate(20)" fill="#fafafa"/>
        <path id="Path-7" data-name="Path" d="M0,0H2V2H0Z" transform="translate(26)" fill="#fafafa"/>
        <path id="Path-8" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 2)" fill="#fafafa"/>
        <path id="Path-9" data-name="Path" d="M0,0H2V2H0Z" transform="translate(12 2)" fill="#fafafa"/>
        <path id="Path-10" data-name="Path" d="M0,0H2V2H0Z" transform="translate(18 2)" fill="#fafafa"/>
        <path id="Path-11" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 2)" fill="#fafafa"/>
        <path id="Path-12" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 4)" fill="#fafafa"/>
        <path id="Path-13" data-name="Path" d="M0,0H2V2H0Z" transform="translate(10 4)" fill="#fafafa"/>
        <path id="Path-14" data-name="Path" d="M0,0H2V2H0Z" transform="translate(16 4)" fill="#fafafa"/>
        <path id="Path-15" data-name="Path" d="M0,0H2V2H0Z" transform="translate(22 4)" fill="#fafafa"/>
        <path id="Path-16" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 4)" fill="#fafafa"/>
        <path id="Path-17" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2 6)" fill="#fafafa"/>
        <path id="Path-18" data-name="Path" d="M0,0H2V2H0Z" transform="translate(8 6)" fill="#fafafa"/>
        <path id="Path-19" data-name="Path" d="M0,0H2V2H0Z" transform="translate(20 6)" fill="#fafafa"/>
        <path id="Path-20" data-name="Path" d="M0,0H2V2H0Z" transform="translate(26 6)" fill="#fafafa"/>
        <path id="Path-21" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 8)" fill="#fafafa"/>
        <path id="Path-22" data-name="Path" d="M4,0H6V2h4V4H8V6H6V4H4V6H2V4H0V2H4Z" transform="translate(10 8)" fill="#fafafa"/>
        <path id="Path-23" data-name="Path" d="M0,0H2V2H0Z" transform="translate(24 8)" fill="#fafafa"/>
        <path id="Path-24" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 8)" fill="#fafafa"/>
        <path id="Path-25" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 10)" fill="#fafafa"/>
        <path id="Path-26" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 10)" fill="#fafafa"/>
        <path id="Path-27" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 10)" fill="#414141"/>
        <path id="Path-28" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2 12)" fill="#fafafa"/>
        <path id="Path-29" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 12)" fill="#414141"/>
        <path id="Path-30" data-name="Path" d="M0,0H4V2H0Z" transform="translate(6 12)" fill="#fafafa"/>
        <path id="Path-31" data-name="Path" d="M0,0H2V2H0Z" transform="translate(10 12)" fill="#414141"/>
        <path id="Path-32" data-name="Path" d="M4,0H6V2H8V4h2V8H0V4H2V2H4Z" transform="translate(10 12)" fill="#414141"/>
        <path id="Path-33" data-name="Path" d="M0,0H2V2H0Z" transform="translate(18 12)" fill="#414141"/>
        <path id="Path-34" data-name="Path" d="M0,0H4V2H0Z" transform="translate(20 12)" fill="#fafafa"/>
        <path id="Path-35" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 12)" fill="#fafafa"/>
        <path id="Path-36" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 14)" fill="#fafafa"/>
        <path id="Path-37" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2 14)" fill="#414141"/>
        <path id="Path-38" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 14)" fill="#fafafa"/>
        <path id="Path-39" data-name="Path" d="M2,0H6V2H4V6H0V2H2Z" transform="translate(4 14)" fill="#414141"/>
        <path id="Path-40" data-name="Path" d="M0,0H2V2H0Z" transform="translate(10 14)" fill="#fafafa"/>
        <path id="Path-41" data-name="Path" d="M0,0H2V2H0Z" transform="translate(18 14)" fill="#fafafa"/>
        <path id="Path-42" data-name="Path" d="M0,0H4V2H6V6H2V2H0Z" transform="translate(20 14)" fill="#414141"/>
        <path id="Path-43" data-name="Path" d="M0,0H2V2H0Z" transform="translate(24 14)" fill="#fafafa"/>
        <path id="Path-44" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 14)" fill="#fafafa"/>
        <path id="Path-45" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 14)" fill="#414141"/>
        <path id="Path-46" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 16)" fill="#414141"/>
        <path id="Path-47" data-name="Path" d="M2,0H4V4H8V0h2V4H20V0h2V4h4V0h2V2h2V4H28V6H16v8H14V6H2V4H0V2H2Z" transform="translate(0 16)" fill="#fafafa"/>
        <path id="Path-48" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 16)" fill="#414141"/>
        <path id="Path-49" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 16)" fill="#fafafa"/>
        <path id="Path-50" data-name="Path" d="M14,0h2V14H4V12H0V4H12V2h2Z" transform="translate(16 18)" fill="#414141"/>
        <path id="Path-51" data-name="Path" d="M0,0H2V2H14v8h2v2H0Z" transform="translate(0 20)" fill="#414141"/>
        <path id="Path-52" data-name="Path" d="M0,0H2V2H0Z" transform="translate(20 28)" fill="#fafafa"/>
        <path id="Path-53" data-name="Path" d="M0,0H4V2H0Z" transform="translate(16 30)" fill="#fafafa"/>
      </svg> 
      :
      <svg className={styles.logo} width="800px" height="800px" viewBox="0 0 32 32" id="umbrellawithraindrops_Dark" data-name="icon-light" xmlns="http://www.w3.org/2000/svg">
        <path id="Path" d="M0,0H2V2H0Z" fill="#fafafa"/>
        <path id="Path-2" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2)" fill="#414141"/>
        <path id="Path-3" data-name="Path" d="M4,0H8V2h2V0h4V2H12V4H10V6H8V8h2V6h2V4h2V2h2V0h4V2H18V4H16V6h2V4h2V2h2V0h4V2h2V0h4V2H30V4H28V6H26V8H24v2h2V8h2V6h2V4h2V8H30v2H28v2h2v2H28v2H26V14H24V12H20V10H16V8H14v2H10v2H6V10H4v2H2v2H0V10H2V8H0V4H2V2H4Z" fill="#fafafa"/>
        <path id="Path-4" data-name="Path" d="M0,0H2V2H0Z" transform="translate(8)" fill="#414141"/>
        <path id="Path-5" data-name="Path" d="M0,0H2V2H0Z" transform="translate(14)" fill="#414141"/>
        <path id="Path-6" data-name="Path" d="M0,0H2V2H0Z" transform="translate(20)" fill="#414141"/>
        <path id="Path-7" data-name="Path" d="M0,0H2V2H0Z" transform="translate(26)" fill="#414141"/>
        <path id="Path-8" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 2)" fill="#414141"/>
        <path id="Path-9" data-name="Path" d="M0,0H2V2H0Z" transform="translate(12 2)" fill="#414141"/>
        <path id="Path-10" data-name="Path" d="M0,0H2V2H0Z" transform="translate(18 2)" fill="#414141"/>
        <path id="Path-11" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 2)" fill="#414141"/>
        <path id="Path-12" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 4)" fill="#414141"/>
        <path id="Path-13" data-name="Path" d="M0,0H2V2H0Z" transform="translate(10 4)" fill="#414141"/>
        <path id="Path-14" data-name="Path" d="M0,0H2V2H0Z" transform="translate(16 4)" fill="#414141"/>
        <path id="Path-15" data-name="Path" d="M0,0H2V2H0Z" transform="translate(22 4)" fill="#414141"/>
        <path id="Path-16" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 4)" fill="#414141"/>
        <path id="Path-17" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2 6)" fill="#414141"/>
        <path id="Path-18" data-name="Path" d="M0,0H2V2H0Z" transform="translate(8 6)" fill="#414141"/>
        <path id="Path-19" data-name="Path" d="M0,0H2V2H0Z" transform="translate(20 6)" fill="#414141"/>
        <path id="Path-20" data-name="Path" d="M0,0H2V2H0Z" transform="translate(26 6)" fill="#414141"/>
        <path id="Path-21" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 8)" fill="#414141"/>
        <path id="Path-22" data-name="Path" d="M4,0H6V2h4V4H8V6H6V4H4V6H2V4H0V2H4Z" transform="translate(10 8)" fill="#414141"/>
        <path id="Path-23" data-name="Path" d="M0,0H2V2H0Z" transform="translate(24 8)" fill="#414141"/>
        <path id="Path-24" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 8)" fill="#414141"/>
        <path id="Path-25" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 10)" fill="#414141"/>
        <path id="Path-26" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 10)" fill="#414141"/>
        <path id="Path-27" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 10)" fill="#fafafa"/>
        <path id="Path-28" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2 12)" fill="#414141"/>
        <path id="Path-29" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 12)" fill="#fafafa"/>
        <path id="Path-30" data-name="Path" d="M0,0H4V2H0Z" transform="translate(6 12)" fill="#414141"/>
        <path id="Path-31" data-name="Path" d="M0,0H2V2H0Z" transform="translate(10 12)" fill="#fafafa"/>
        <path id="Path-32" data-name="Path" d="M4,0H6V2H8V4h2V8H0V4H2V2H4Z" transform="translate(10 12)" fill="#fafafa"/>
        <path id="Path-33" data-name="Path" d="M0,0H2V2H0Z" transform="translate(18 12)" fill="#fafafa"/>
        <path id="Path-34" data-name="Path" d="M0,0H4V2H0Z" transform="translate(20 12)" fill="#414141"/>
        <path id="Path-35" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 12)" fill="#414141"/>
        <path id="Path-36" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 14)" fill="#414141"/>
        <path id="Path-37" data-name="Path" d="M0,0H2V2H0Z" transform="translate(2 14)" fill="#fafafa"/>
        <path id="Path-38" data-name="Path" d="M0,0H2V2H0Z" transform="translate(4 14)" fill="#414141"/>
        <path id="Path-39" data-name="Path" d="M2,0H6V2H4V6H0V2H2Z" transform="translate(4 14)" fill="#fafafa"/>
        <path id="Path-40" data-name="Path" d="M0,0H2V2H0Z" transform="translate(10 14)" fill="#414141"/>
        <path id="Path-41" data-name="Path" d="M0,0H2V2H0Z" transform="translate(18 14)" fill="#414141"/>
        <path id="Path-42" data-name="Path" d="M0,0H4V2H6V6H2V2H0Z" transform="translate(20 14)" fill="#fafafa"/>
        <path id="Path-43" data-name="Path" d="M0,0H2V2H0Z" transform="translate(24 14)" fill="#414141"/>
        <path id="Path-44" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 14)" fill="#414141"/>
        <path id="Path-45" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 14)" fill="#fafafa"/>
        <path id="Path-46" data-name="Path" d="M0,0H2V2H0Z" transform="translate(0 16)" fill="#fafafa"/>
        <path id="Path-47" data-name="Path" d="M2,0H4V4H8V0h2V4H20V0h2V4h4V0h2V2h2V4H28V6H16v8H14V6H2V4H0V2H2Z" transform="translate(0 16)" fill="#414141"/>
        <path id="Path-48" data-name="Path" d="M0,0H2V2H0Z" transform="translate(28 16)" fill="#fafafa"/>
        <path id="Path-49" data-name="Path" d="M0,0H2V2H0Z" transform="translate(30 16)" fill="#414141"/>
        <path id="Path-50" data-name="Path" d="M14,0h2V14H4V12H0V4H12V2h2Z" transform="translate(16 18)" fill="#fafafa"/>
        <path id="Path-51" data-name="Path" d="M0,0H2V2H14v8h2v2H0Z" transform="translate(0 20)" fill="#fafafa"/>
        <path id="Path-52" data-name="Path" d="M0,0H2V2H0Z" transform="translate(20 28)" fill="#414141"/>
        <path id="Path-53" data-name="Path" d="M0,0H4V2H0Z" transform="translate(16 30)" fill="#414141"/>
      </svg>
    }
    </div>
  )
}

export default Logo