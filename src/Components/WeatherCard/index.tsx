import styles from './index.module.scss'
import { useAppSelector, useAppDispatch } from 'store/hooks';
import { selectWeatherArray, setWeatherArray } from 'store/weatherarray/weatherarraySlice';
import HoursLine from './HoursLine';
import FiveDays from './FiveDays';
import { selectCityName } from 'store/city/citySlice';
import { selectFiveDays, fiveDaysChange } from 'store/fiveDays/fiveDaysSlice';
import { useEffect } from 'react'
import moment from 'moment';
import 'moment/locale/ru';
import  { IDailyWeather, IDailyForecastArray }  from 'api/types';

function WeatherCard() {
  const dispatch = useAppDispatch();
  const weatherArray = useAppSelector(selectWeatherArray);
  const fiveDays = useAppSelector(selectFiveDays);
  const cityName = useAppSelector(selectCityName);
  console.log(weatherArray)
  function iconSrc(ico: string): string {
    return 'https://openweathermap.org/img/wn/'+ico+'@2x.png'
  }

  const checkHandler = () => {
    fiveDays === true && showDefaultDay()
    dispatch(fiveDaysChange())
  }

  function showDefaultDay() {
    let newDay: IDailyForecastArray = {list: [], city: weatherArray.city}
    let bla1: {day: number; weather: IDailyWeather[]; show: boolean;}[] = []
    weatherArray.list.map((value, index) => {
      let dd: {day: number; weather: IDailyWeather[]; show: boolean;} = {day: value.day, weather: value.weather, show: false}
      if (index === 0) {
        dd.show = true
      }
      bla1.push(dd)
    })
    newDay.list = bla1
    dispatch(setWeatherArray(newDay))
  }

  useEffect(() => {
    localStorage.setItem('fiveDays', fiveDays.toString())
  }, [ fiveDays ])

  function dayTemperature(days: IDailyWeather[]): number {
    let temp: number = 0
    let allTemp: number = 0
    days.forEach((d: IDailyWeather) => {
      allTemp += Math.round((d.main.temp_min+d.main.temp_max)/2)
    })
    temp = Math.round(allTemp / days.length)
    return temp
  }

  return (
    <div className={styles.maincard}>
        {weatherArray.list.map((value)=>(
          value.show &&
          <div key={value.day} className={styles.currentDay}>
            <div className={styles.currentDayWeather}>
              <div>
                <div >{weatherArray.list[0].day === value.day
                  ? moment.unix(value.weather[0].dt).locale('ru-ru').format('D MMMM YYYY HH:mm') 
                  : moment.unix(value.weather[0].dt).locale('ru-ru').format('D MMMM YYYY')}</div>
                <div className={styles.currentCity}>{cityName}</div>
              </div>
              <div>
                <div className={styles.currentDescription}>{value.weather[0].weather[0].description}</div>
                <div className={styles.currentFeelsLike}>Ощущается как: {Math.round(value.weather[0].main.feels_like)}°C</div>
                <div className={styles.currentFeelsLike}>Атмосферное давление: {Math.round(value.weather[0].main.pressure)} hPa</div>
                <div className={styles.currentFeelsLike}>Влажность воздуха: {Math.round(value.weather[0].main.humidity)}%</div>
              </div>
            </div>
              <div className={styles.currentDegree}>{weatherArray.list[0].day === value.day 
                ? Math.round((value.weather[0].main.temp_max+value.weather[0].main.temp_min)/2)
                : dayTemperature(value.weather)}°C</div>
            <div className={styles.currentWeatherIconCover}>
              <img src={iconSrc(value.weather[0].weather[0].icon)}></img>
              <div>
                <label>
                  Прогноз на 5 дней
                  <input 
                    type="checkbox" 

                    id="checkbox"
                    checked={fiveDays}
                    onChange={checkHandler}
                    readOnly
                  />
                </label>
              </div>
            </div>
          </div>
        ))}
      <HoursLine/>
      {fiveDays ? <FiveDays/> : <></>}
    </div>
  )
}

export default WeatherCard