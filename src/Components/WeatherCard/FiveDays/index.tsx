import styles from './index.module.scss'
import { useAppSelector, useAppDispatch } from 'store/hooks';
import { selectWeatherArray, setWeatherArray } from 'store/weatherarray/weatherarraySlice';
import  { IDailyWeather, IDailyForecastArray }  from 'api/types';
import moment from 'moment';
import 'moment/locale/ru';

function FiveDays() {

  const dispatch = useAppDispatch();
  const weatherArray = useAppSelector(selectWeatherArray);
 
  function dayTemperature(days: IDailyWeather[]): number {
    let temp: number = 0
    let allTemp: number = 0
    days.forEach((d: IDailyWeather) => {
      allTemp += Math.round((d.main.temp_min+d.main.temp_max)/2)
    })
    temp = Math.round(allTemp / days.length)
    return temp
  }

  function iconSrc(ico: string): string {
    return 'https://openweathermap.org/img/wn/'+ico+'@2x.png'
  }

  function chooseDay(day: number) {
    let newDay: IDailyForecastArray = {list: [], city: weatherArray.city}
    let bla1: {day: number; weather: IDailyWeather[]; show: boolean;}[] = []
    weatherArray.list.map((value, index) => {
      let dd: {day: number; weather: IDailyWeather[]; show: boolean;} = {day: value.day, weather: value.weather, show: false}
      if (value.day === day) {
        dd.show = true
      }
      bla1.push(dd)
    })
    newDay.list = bla1
    dispatch(setWeatherArray(newDay))

  }

  return (
    <div className={styles.weatherdaysline}>
      {weatherArray.list.map((value, index) => (
        index !== 5 &&
          <div className={styles.weatherdays} key={value.day} onClick={() => chooseDay(value.day)}>
            <div className={styles.weatherDate}>
              {moment.unix(value.weather[0].dt).locale('ru-ru').format('D MMMM YYYY')}
            </div>
            <div className={styles.degreeContainer}>
              <div className={styles.weatherDegry}>
                {dayTemperature(value.weather)+'°C'}
              </div>
                <img className={styles.weatherIcon}src={iconSrc(value.weather[0].weather[0].icon)}></img>
            </div>
          </div>
      ))}
  </div>
  )
}

export default FiveDays