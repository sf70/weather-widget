import { useEffect } from 'react'
import { useAppSelector } from 'store/hooks';
import { selectWeatherArray } from 'store/weatherarray/weatherarraySlice';
import { IDailyWeather }  from 'api/types';
import styles from './index.module.scss'
import moment from 'moment';
import 'moment/locale/ru';

function HoursLine() {
  const weatherArray = useAppSelector(selectWeatherArray);
  let dayWeather: Array<IDailyWeather> = [...weatherArray.list[0].weather, ...weatherArray.list[1].weather];

  useEffect(() => {
    dayWeather = []
    dayWeather = [...weatherArray.list[0].weather, ...weatherArray.list[1].weather]
  }, [ weatherArray ])
  
  function iconSrc(ico: string): string {
    return 'https://openweathermap.org/img/wn/'+ico+'@2x.png'
  }

  return (
    <div className={styles.weatherhoursline}>
      {dayWeather.map((hour, index) => (
        index <= 7 &&
        <div
          key={index}
        >
          <div className={styles.weatherhours}>
            <div className={styles.degreeContainer}>
              <div className={styles.weatherDegry}>
                {Math.round((hour.main.temp_max+hour.main.temp_min)/2)+'°C'}
              </div>
              <img  className={styles.weatherIcon}src={iconSrc(hour.weather[0].icon)}></img>
            </div>
            <div className={styles.weatherDate}>
              {moment.unix(hour.dt).locale('ru-ru').format('HH:mm D.MM.YYYY')}
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}

export default HoursLine