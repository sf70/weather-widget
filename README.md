# Запуск проекта
```bash
yarn install && yarn start
```

#### With docker
```bash
docker build -t weather:1.0.0 .
docker run -it -d --rm -p 8080:8080 --name weather-widget weather:1.0.0
```

далее перейти на http://127.0.0.1:8080/

для остановки контейнера
```bash
docker kill weather-widget
```